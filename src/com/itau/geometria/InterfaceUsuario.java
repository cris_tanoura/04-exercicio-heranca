package com.itau.geometria;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.itau.geometria.formas.Circulo;
import com.itau.geometria.formas.Quadrado;
import com.itau.geometria.formas.Triangulo;

public class InterfaceUsuario {

	
	static Scanner scan = new Scanner(System.in);
	
	public static List<Double> obterInterface(int vezes) {
		List<Double> listaLados = new ArrayList<>();
			
		for (int i=0 ; i < vezes; i++) {
			System.out.println("Digite o tamanho do lado ou 0 para finalizar: ");
			// . next so pega uma palavra antes do primeiro espaco
			double lado = scan.nextDouble();
			String ladoString = scan.nextLine();
			double entradaDouble = Double.parseDouble(ladoString);
			if (lado != 0) {
				listaLados.add(entradaDouble);
			} else {
				vezes = i;
			}
		}
		
		return listaLados;
	}
	

}