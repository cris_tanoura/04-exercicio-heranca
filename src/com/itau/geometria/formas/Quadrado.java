package com.itau.geometria.formas;

public class Quadrado extends Forma {
	int ladoA;
	int ladoB;
	
	
	@Override
	public double calcularArea() {
		// TODO Auto-generated method stub
		return ladoA * ladoB;
	}
	public Quadrado(int ladoA, int ladoB) {
		this.ladoA = ladoA;
		this.ladoB = ladoB;
		
	}
	// permite que outras classes tenham acesso a variavel
	public double getLadoA() {
		return ladoA;
	}
}
