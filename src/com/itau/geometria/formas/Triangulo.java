package com.itau.geometria.formas;

public class Triangulo extends Forma {
	int ladoA;
	int ladoB;
	int ladoC;
	
	@Override
	public double calcularArea() {
		// TODO Auto-generated method stub
		double s = (ladoA + ladoB + ladoC) / 2;       
		return Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC));
	}
	
	public Triangulo (int ladoA, int ladoB, int LadoC) throws Exception {
	
		boolean trianguloValido = false;
		trianguloValido = (ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA ;
		if (trianguloValido == true) {
			this.ladoA = ladoA;
			this.ladoB = ladoB;
			this.ladoC = ladoC;	
		}
		else {
			System.out.println("Os lados do trinangulos sao invalidos");
			throw new Exception("Lados nao formam um triangulo");
		}
		
	}
	
	
}

