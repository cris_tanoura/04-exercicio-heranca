package com.itau.geometria;

import com.itau.geometria.formas.Circulo;
import com.itau.geometria.formas.Quadrado;
import com.itau.geometria.formas.Triangulo;

public class ConstrutorFormas {

	public static String verificarForma(int quantidadeLados, int tamanhoLadoA, int tamanhoLadoB, int tamanhoLadoC) {
		String respostaArea = "";
		
		switch (quantidadeLados) {
		case 1:
			Circulo circulo = new Circulo(tamanhoLadoA);
			double areaCirculo = 0;
			areaCirculo = circulo.calcularArea();
			respostaArea = "Area circulo: " + areaCirculo;
			break;
		case 2:
			Quadrado quadrado = new Quadrado(tamanhoLadoA, tamanhoLadoB);
			double areaQuadrado = 0;
			areaQuadrado = quadrado.calcularArea();
			respostaArea = "Area quadrado: " + areaQuadrado;
			break;
		case 3:
			try {
				Triangulo triangulo = new Triangulo(tamanhoLadoA, tamanhoLadoB, tamanhoLadoC);
				double areaTriangulo = 0;
				areaTriangulo = triangulo.calcularArea();
				respostaArea = "Area triangulo: " + areaTriangulo;
			} catch (Exception e) {
			}
			break;
		default:
			respostaArea = "numero de lados diferente de 1, 2 , 3";
		}
		return respostaArea;
	}
}
